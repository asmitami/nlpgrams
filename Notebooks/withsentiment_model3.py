from __future__ import print_function
from keras.models import Model
from keras.layers import Input, LSTM, Dense, Bidirectional, Concatenate
# from attention_keras.layers.attention import AttentionLayer
from keras_self_attention import SeqSelfAttention
from keras.layers.core import Reshape
from keras import optimizers 
import random
import numpy as np
import pandas as pd
import json
# import Concatenate

batch_size = 64  # Batch size for training.
epochs = 100  # Number of epochs to train for.
latent_dim = 256  # Latent dimensionality of the encoding space.
num_samples = 10000  # Number of samples to train on.
# Path to the data txt file on disk.
data_path = '/Users/manjari/USC_coursework/csci-544/project/data /tf_idf_data_917_lower_dimension'
summary_path = '/Users/manjari/USC_coursework/csci-544/project/data /summary_new'
genre_path = '/Users/asmitamitra/Documents/Projects/NLP/nlpgrams/Data/movie_genre_data.json'
sentiment_path = '/Users/manjari/USC_coursework/csci-544/project/data /entireMovieSentiments'


# Vectorize the data
movie_list = []
screenplay_words = set()
movie_wcount = {}
movie_screenplay_words = {}
movie_summary_words = {}
summary_words = set()
summary_list = {}

df = pd.read_csv(data_path)
#df = df[:200]
df.drop('Unnamed: 0', axis=1, inplace=True)
#vocab
screenplay_words = df.columns[1:]
screenplay_words = list(screenplay_words)
screenplay_words.append('NULL')
len(screenplay_words)

def get_column_name(row):
    return (df.columns[(df != 0.0).iloc[row.name]][1:]).tolist()

df_new = pd.DataFrame()
df_new["words"] = df.apply(get_column_name, axis=1)
df_new['movie_name']= df['file name']
movie_list = df['file name']
movie_screenplay_words = df_new.set_index('movie_name')['words'].to_dict()

def get_tf_idf(row):
  return (df.columns[(df != 0.0).iloc[row.name][0:]]).to_dict()
df_new1 = df.T.to_dict()

movie_name_list = df['file name'].tolist()
tf_idf_dict = dict(zip(movie_name_list, list(df_new1.values())))

max_screenplay_words = max([len(value) for key,value in movie_screenplay_words.items()])

"""###Reading sentiment"""
df_senti = pd.read_csv(sentiment_path , encoding="utf-8")
df_senti.drop('Unnamed: 0', axis=1, inplace=True)
df_new_senti = df_senti.loc[df_senti['movieName'].isin(movie_list)]

df_new_senti=df_new_senti.iloc[:,1:]

df_senti_dict = df_new_senti.T.to_dict('list')

"""### Reading the summaries"""
df_sum = pd.read_csv(summary_path , encoding="utf-8")

import re
def remove_punctuations(s1):
    punct = r'[~{}^*+=|<>%&\-"/;:,.\n\t?!()\]\[_@#$\'\,`]+'
    punct_words = re.findall(punct,s1)
    for p in punct_words:
      s1 = s1.replace(p," ")
    return s1

# Preprocessing the summaries
def get_words_sum(row):
    # print(row[1])
    temp_dict = eval(row[1])
    allwords = remove_punctuations("".join(temp_dict.values()).lower())
    words = allwords.split()
    return words

df_sum["sum"]= pd.DataFrame(df_sum.apply(get_words_sum,axis=1))


"""Convert Summaries to Dict and Unique words"""
summary_temp_dict=df_sum.set_index('Unnamed: 0')['sum'].to_dict()
summary_temp_dict
movie_list = list(movie_list)
summary_dict = dict()

for k,v in summary_temp_dict.items():
  if k in movie_list:
    v.insert(0,"**START**")
    v.append('**END**')
    summary_dict[k] = v
summary_dict

summary_words = set()
for k,v in summary_dict.items():
  for i in v:
    summary_words.add(i)
summary_words.add('NULL')

max_summary_length = max([len(value) for key,value in summary_dict.items()])
max_summary_length

"""##Comparison Summaries"""
comparison_summary_dict = {}
for movie, wordlist in summary_temp_dict.items():
    comparison_summary_dict[movie] = ""
    for word in wordlist:
        if word != '' and word != ' ':
            comparison_summary_dict[movie] += word + " "

"""##Taking tf--idf as summary values"""

# df_summary = pd.read_csv("/content/drive/Shared drives/NLP_Grams/Data/scriptbase-master/summary_tf_idf")

# df_summary.drop('Unnamed: 0', axis=1, inplace=True)

# df_summ_new = df_summary.T.to_dict()

# all_movie_name_list = df_summary['file name'].tolist()

# summ_tf_idf_dict = dict(zip(all_movie_name_list, list(df_summ_new.values())))

"""## Model Building"""



screenplay_words = sorted(list(screenplay_words))
screenplay_words.extend(["sent1","sent2","sent3","sent4"])
summary_words = sorted(list(summary_words))

num_encoder_tokens = len(screenplay_words)
num_decoder_tokens = len(summary_words) 
max_encoder_seq_length = max_screenplay_words
max_decoder_seq_length = max_summary_length

print('Number of samples:', len(movie_list))
print('Number of unique input tokens:', num_encoder_tokens)
print('Number of unique output tokens:', num_decoder_tokens)
print('Max sequence length for inputs:', max_encoder_seq_length)
print('Max sequence length for outputs:', max_decoder_seq_length)

input_token_index = dict([(word, i) for i, word in enumerate(screenplay_words)])
target_token_index = dict([(word, i) for i, word in enumerate(summary_words)])

encoder_input_data = np.zeros((len(movie_list), max_encoder_seq_length, num_encoder_tokens),dtype='float32')
decoder_input_data = np.zeros((len(movie_list), max_decoder_seq_length, num_decoder_tokens),dtype='float32')
decoder_target_data = np.zeros((len(movie_list), max_decoder_seq_length, num_decoder_tokens),dtype='float32')

# MAIN ENCODER INITIALIZATION LOOP
for i, movie in enumerate(movie_screenplay_words):
    for t, word in enumerate(movie_screenplay_words[movie]):
        encoder_input_data[i, t, input_token_index[word]] = tf_idf_dict[movie][word] * 10
    encoder_input_data[i, t + 1:, input_token_index['NULL']] = 0
    encoder_input_data[i,-4, input_token_index['sent1']] = df_senti_dict[i][0]
    encoder_input_data[i,-3, input_token_index['sent2']] = df_senti_dict[i][1]
    encoder_input_data[i,-2, input_token_index['sent3']] = df_senti_dict[i][2]
    encoder_input_data[i,-1, input_token_index['sent4']] = df_senti_dict[i][3]

encoder_input_data.shape


encoder_input_data.shape

# DECODER INITIALIZATION LOOP
for i,movie in enumerate(movie_summary_words):
    for t, word in enumerate(movie_summary_words[movie]):
        # decoder_target_data is ahead of decoder_input_data by one timestep
        decoder_input_data[i, t, target_token_index[word]] = 1
        if t > 0:
            # decoder_target_data will be ahead by one timestep
            # and will not include the start character.
            decoder_target_data[i, t - 1, target_token_index[word]] = 1
    decoder_input_data[i, t + 1:, target_token_index['NULL']] = 0
    decoder_target_data[i, t:, target_token_index['NULL']] = 0

# Define an input sequence and process it.
encoder_inputs = Input(shape=(None, num_encoder_tokens))
encoder = Bidirectional(LSTM(latent_dim, return_state=True))
encoder_outputs, forward_h, forward_c, backward_h, backward_c = encoder(encoder_inputs)

state_h = Concatenate()([forward_h, backward_h])
state_c = Concatenate()([forward_c, backward_c])


encoder_states = [state_h, state_c]


decoder_inputs = Input(shape=(None, num_decoder_tokens))
decoder_lstm = LSTM(latent_dim * 2, return_sequences=True, return_state=True)
decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
decoder_dense = Dense(num_decoder_tokens, activation='sigmoid')
decoder_outputs = decoder_dense(decoder_outputs)
#print("decoder ", decoder_outputs)

# Model Defining
model = Model([encoder_inputs, decoder_inputs], decoder_outputs)
sgd = optimizers.SGD(lr=0.0008, decay=0.0, momentum=0.9, nesterov=True)
model.compile(optimizer='adam', loss='categorical_crossentropy',metrics=['accuracy'])
model.fit([encoder_input_data, decoder_input_data], decoder_target_data, batch_size=batch_size, epochs= 10, validation_split=0.2)
print("---------------------------------------------------------------------")
from keras.utils import plot_model
plot_model(model, show_shapes=True, show_layer_names=True, to_file='model.png')
from IPython.display import Image
Image(retina=True, filename='model.png')
# Save model
# model.save('s2s.h5')

encoder_model = Model(encoder_inputs, encoder_states)
decoder_state_input_h = Input(shape=(latent_dim * 2,))
decoder_state_input_c = Input(shape=(latent_dim * 2,))
decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
decoder_outputs, state_h, state_c = decoder_lstm(decoder_inputs, initial_state=decoder_states_inputs)

decoder_states = [state_h, state_c]
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = Model([decoder_inputs] + decoder_states_inputs,[decoder_outputs] + decoder_states) 


#reversse the input/target sequence
reverse_input_word_index = dict((i, word) for word, i in input_token_index.items())
reverse_target_word_index = dict((i, word) for word, i in target_token_index.items())

def test_sentence(input_text):
    # Encode the input as state vectors.
    states_value = encoder_model.predict(input_text)
    # print("states_value ======", states_value)

    result_seq = np.zeros((1, 1, num_decoder_tokens))
    result_seq[0, 0, target_token_index['**START**']] = 1.

    stopper = False
    result_sentence = ''
    while not stopper:
        output_tokens, h, c = decoder_model.predict([result_seq] + states_value)

        # t_index = np.argmax(output_tokens[0, -1, :])
        n = np.max(output_tokens[0, -1, :])
        vals = np.where(output_tokens[0, -1, :] == n)
        t_index = random.choice(vals)[0]
        test_word = reverse_target_word_index[t_index]

        result_sentence += test_word + " "

        #stop condition
        if (test_word == '**END**' or len(result_sentence) > max_decoder_seq_length):
            stopper = True

        #updating the target and states
        result_seq = np.zeros((1, 1, num_decoder_tokens))
        result_seq[0, 0, t_index] = 1.
        states_value = [h, c]

    return result_sentence

# TESTING 

# test data
test_encoder_data = encoder_input_data[-100:]
m = len(test_encoder_data)
seq_index = 0
output_data = {}
while seq_index < len(test_encoder_data):
    input_text = test_encoder_data[seq_index:seq_index+1]
    #print(input_text)
    result = test_sentence(input_text)
    #print("Result summary : ", result)
    output_data[movie_list[seq_index]] = result
    #print("Actual summary: ", comparison_summary_dict[movie_list[seq_index]])
    seq_index += 1

json_data= {"Result summary":output_data,"Actual summary":comparison_summary_dict }
with open("output_summaries.json", 'w') as f2:
    json.dump(json_data, f2, indent=2)


