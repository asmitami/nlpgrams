import json
from rouge import Rouge

with open('/Users/manjari/USC_coursework/csci-544/project_word_embeddings/output_summaries.json') as f:
    d=json.load(f)

actual=[]

predicted=[]
for k,v in d["Result summary"].items():
    if k in d["Actual summary"].keys():
        if d["Actual summary"][k]!= "":
            predicted.append(v)
            actual.append(d["Actual summary"][k])

print(len(actual))
print(len(predicted))

rouge = Rouge()

scores = rouge.get_scores(actual, predicted, avg=True)
print(scores)