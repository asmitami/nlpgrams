from __future__ import print_function
from keras.models import Model
from keras.layers import Input, LSTM, Dense
import numpy as np
import pandas as pd
import json
import re

batch_size = 32 # Batch size for training.
epochs = 10  # Number of epochs to train for.
latent_dim = 256  # Latent dimensionality of the encoding space.
num_samples = 10000  # Number of samples to train on.
# Path to the data txt file on disk.
data_path = '/Users/manjari/USC_coursework/csci-544/nlpgrams/Data/tf_idf_data_200_lower_dimension'
summary_path = '/Users/manjari/USC_coursework/csci-544/nlpgrams/Data/summary_new.csv'

# Vectorize the data
movie_list = []
screenplay_words = set()
movie_wcount = {}
movie_screenplay_words = {}
movie_summary_words = {}
summary_words = set()
summary_list = {}

df = pd.read_csv(data_path)

df.drop('Unnamed: 0', axis=1, inplace=True)

#vocab
screenplay_words = df.columns[1:]

#len(screenplay_words)
screenplay_words= list(screenplay_words)

#screenplay_words.insert(len(screenplay_words),'NULL')
screenplay_words.append('NULL')


def get_column_name(row):
    # print(df.columns[(df != 0.0).iloc[row.name]])
    return (df.columns[(df != 0.0).iloc[row.name]][1:]).tolist()

df_new = pd.DataFrame()
df_new["words"] = df.apply(get_column_name, axis=1)

df_new['movie_name']= df['file name']

movie_list = df['file name']


movie_screenplay_words = df_new.set_index('movie_name')['words'].to_dict()


movie_screenplay_words['Remember Me (2010 film)']

max_screenplay_words = max([len(value) for key,value in movie_screenplay_words.items()])


"""### Reading the summaries"""

df_sum = pd.read_csv(summary_path , encoding="utf-8")
df_sum.shape

def remove_punctuations(s1):
  punct = r'[~{}*^+=|<>%&\-"/;:,!()\]\[_@#$]+'
  punct_words = re.findall(punct,s1)
  for p in punct_words:
      s1=s1.replace(p," ")
  return s1

def get_words_sum(row):

  temp_dict=eval(row[1])
  s1 = "".join(temp_dict.values())
  words= s1.split(" ")
  return words

df_sum["sum"]= pd.DataFrame(df_sum.apply(get_words_sum,axis=1))
summary_temp_dict=df_sum.set_index('Unnamed: 0')['sum'].to_dict()
movie_list = list(movie_list)

summary_dict = dict()
for k,v in summary_temp_dict.items():
  if k in movie_list:
    v.insert(0,"**START**")
    v.append('**END**')
    summary_dict[k] = v

summary_words = set()
for k,v in summary_dict.items():
  for i in v:
    summary_words.add(i)
summary_words.add('NULL')
max_summary_length = max([len(value) for key,value in summary_dict.items()])

"""## Model Building"""

screenplay_words = sorted(list(screenplay_words))
summary_words = sorted(list(summary_words))
num_encoder_tokens = len(screenplay_words)
num_decoder_tokens = len(summary_words)
max_encoder_seq_length = max_screenplay_words
max_decoder_seq_length = max_summary_length

print('Number of samples:', len(movie_list))
print('Number of unique input tokens:', num_encoder_tokens)
print('Number of unique output tokens:', num_decoder_tokens)
print('Max sequence length for inputs:', max_encoder_seq_length)
print('Max sequence length for outputs:', max_decoder_seq_length)

input_token_index = dict([(word, i) for i, word in enumerate(screenplay_words)])
target_token_index = dict([(word, i) for i, word in enumerate(summary_words)])

encoder_input_data = np.zeros((len(movie_list), max_encoder_seq_length, num_encoder_tokens),dtype='float32')
decoder_input_data = np.zeros((len(movie_list), max_decoder_seq_length, num_decoder_tokens),dtype='float32')
decoder_target_data = np.zeros((len(movie_list), max_decoder_seq_length, num_decoder_tokens),dtype='float32')
print(encoder_input_data.shape)
print(decoder_input_data.shape)

for i, movie in enumerate(movie_screenplay_words):
    for t, word in enumerate(movie_screenplay_words[movie]):
        encoder_input_data[i, t, input_token_index[word]] = 1.
    encoder_input_data[i, t + 1:, input_token_index['NULL']] = 1.
for i,movie in enumerate(summary_dict):
    for t, word in enumerate(summary_dict[movie]):
        # decoder_target_data is ahead of decoder_input_data by one timestep
        if word == ' ':
            print(movie)
        decoder_input_data[i, t, target_token_index[word]] = 1.
        if t > 0:
            # decoder_target_data will be ahead by one timestep
            # and will not include the start character.
            decoder_target_data[i, t - 1, target_token_index[word]] = 1.
    decoder_input_data[i, t + 1:, target_token_index['NULL']] = 1.
    decoder_target_data[i, t:, target_token_index['NULL']] = 1.

#Saving
# np.savez('encoder_input_data.npz', encoder_input_data)
# np.savez('decoder_input_data.npz', decoder_input_data)
# np.savez('decoder_target_data.npz', decoder_target_data)

encoder_inputs = Input(shape=(None, num_encoder_tokens))
encoder = LSTM(latent_dim, return_state=True)
encoder_outputs, state_h, state_c = encoder(encoder_inputs)

# discard the output
encoder_states = [state_h, state_c]
print("encoder sates", encoder_states)

decoder_inputs = Input(shape=(None, num_decoder_tokens))
decoder_lstm = LSTM(latent_dim, return_sequences=True, return_state=True)
decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
decoder_dense = Dense(num_decoder_tokens, activation='softmax')
decoder_outputs = decoder_dense(decoder_outputs)
print("decoder ", decoder_outputs)

# Model Defining
model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

model.compile(optimizer='rmsprop', loss='categorical_crossentropy',metrics=['accuracy'])
model.fit([encoder_input_data, decoder_input_data], decoder_target_data, batch_size=batch_size,epochs=epochs, validation_split=0.2)
print("---------------------------------------------------------------------")
# Save model
model.save('s2s.h5')

#Testing-------------------------------------------------


encoder_model = Model(encoder_inputs, encoder_states)

decoder_state_input_h = Input(shape=(latent_dim,))
decoder_state_input_c = Input(shape=(latent_dim,))
decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
decoder_outputs, state_h, state_c = decoder_lstm(decoder_inputs, initial_state=decoder_states_inputs)
decoder_states = [state_h, state_c]
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = Model([decoder_inputs] + decoder_states_inputs,[decoder_outputs] + decoder_states)

#reversse the input/target sequence
reverse_input_word_index = dict((i, word) for word, i in input_token_index.items())
reverse_target_word_index = dict((i, word) for word, i in target_token_index.items())

def test_sentence(input_text):
    # Encode the input as state vectors.
    states_value = encoder_model.predict(input_text)
    # print("states_value ======", states_value)

    result_seq = np.zeros((1, 1, num_decoder_tokens))
    result_seq[0, 0, target_token_index['**START**']] = 1.

    stopper = False
    result_sentence = ''
    while not stopper:
        output_tokens, h, c = decoder_model.predict([result_seq] + states_value)

        t_index = np.argmax(output_tokens[0, -1, :])
        test_word = reverse_target_word_index[t_index]
        print(test_word)
        result_sentence += test_word + " "

        #stop condition
        if (test_word == '**END**' or len(result_sentence) > max_decoder_seq_length):
            stopper = True

        #updating the target and states
        result_seq = np.zeros((1, 1, num_decoder_tokens))
        result_seq[0, 0, t_index] = 1.
        states_value = [h, c]

    return result_sentence

#test data
input_text = encoder_input_data[0:1]
print(input_text)
result = test_sentence(input_text)
print("decoded sem=ntence",result)
print("end")

