

from __future__ import print_function
from keras.models import Model
from keras.layers import Input, LSTM, Dense, TimeDistributed, concatenate
from keras import optimizers 
import random
import numpy as np
import pandas as pd
import json
from sklearn.model_selection import train_test_split
# from attention_keras.layers.attention import AttentionLayer
from attention_decoder import AttentionDecoder


batch_size = 32  # Batch size for training.
epochs = 100  # Number of epochs to train for.
latent_dim = 256  # Latent dimensionality of the encoding space.
num_samples = 10000  # Number of samples to train on.
# Path to the data txt file on disk.
data_path = '/Users/manjari/USC_coursework/csci-544/project/data /tf_idf_data_917_high_dimension'
summary_path = '/Users/manjari/USC_coursework/csci-544/project/data /summary_new'
# genre_path = '/Users/asmitamitra/Documents/Projects/NLP/nlpgrams/Data/movie_genre_data.json'
sentiment_path = '/Users/manjari/USC_coursework/csci-544/project/data /entireMovieSentiments'

# Vectorize the data
movie_list = []
screenplay_words = set()
movie_wcount = {}
movie_screenplay_words = {}
movie_summary_words = {}
summary_words = set()
summary_list = {}

def scenes_tokenize(row):
  sentence = ""
  x=eval(row)
  for i in x.values():
    for w in i:
      sentence= sentence + w.lower() +" "
  #sentence = sentence.lower()
  return sentence[:-1]

def summ_tokenize(row):
  sentence = ""
  x=eval(row)
  return "".join(list(x.values())).lower()
  #sentence = sentence.lower()
  #return sentence[:-1]

df = pd.read_csv("/Users/manjari/USC_coursework/csci-544/project/data /scene_data", encoding='utf-8',sep=',')
df = df.sort_values("Unnamed: 0")
df = df[:200]
df_summ = pd.read_csv(summary_path, encoding='utf-8',sep=',')
df_summ = df_summ.sort_values("Unnamed: 0")
df_summ['summ'] = df_summ['summaries'].apply(summ_tokenize)

df_summ
df['scenes'] = df['scenes'].apply(scenes_tokenize)

all_movie_data = df.set_index('Unnamed: 0')['scenes'].to_dict()
all_movie_set = set(list(all_movie_data.keys()))

all_summary_data = df_summ.set_index('Unnamed: 0')['summ'].to_dict()
new_summary_dict = {}
for movie,summ in all_summary_data.items():
  if movie in all_movie_set:
    new_summary_dict[movie] = summ
new_summary_dict.keys()

df

new_summary_df = pd.DataFrame(new_summary_dict.items(), columns=["Movie", "summ"])
new_summary_df.shape
df_summ = new_summary_df


import nltk
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

import re
import string
import numpy as np
import pandas as pd
from sklearn.manifold import TSNE

# KERAS
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Flatten, LSTM, Conv1D, MaxPooling1D, Dropout, Activation , RepeatVector
from keras.layers.embeddings import Embedding

import string
def clean_text(text):
    
    ## Remove puncuation
    text = text.translate(string.punctuation)
    
    ## Convert words to lower case and split them
    text = text.lower().split()
    
    ## Remove stop words
    # stops = set(stopwords.words("english"))
    stops = []
    text = [w for w in text if not w in stops and len(w) >= 3]
    
    text = " ".join(text)

    # Clean the text
    text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ", text)
    text = re.sub(r"\+", " ", text)
    text = re.sub(r"\-", " ", text)
    text = re.sub(r"\=", " ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    # text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)
    
    # text = text.split()
    # stemmer = SnowballStemmer('english')
    # stemmed_words = [stemmer.stem(word) for word in text]
    # text = " ".join(stemmed_words)

    return text

def addStartEnd(row):
    str1 = "<SOS> " + row + " <EOS>"
    return str1

df['scenes'] = df['scenes'].map(lambda x: clean_text(x))

df_summ['summ'] = df_summ['summ'].map(lambda x: clean_text(x))
df_summ['summ'] = df_summ['summ'].map(lambda x: addStartEnd(x))

# print(df_summ.head())

df.head()

MAX_LEN =1000

# MOVIE
vocabulary_size = 20000
tokenizer = Tokenizer(num_words = vocabulary_size)
tokenizer.fit_on_texts(df['scenes'])

sequences = tokenizer.texts_to_sequences(df['scenes'])
data = pad_sequences(sequences, maxlen=MAX_LEN)

#SUMMARY
summary_vocab_size = 7000
tokenizer_summary = Tokenizer(num_words = summary_vocab_size)
tokenizer_summary.fit_on_texts(df_summ['summ'])

sequences_summary = tokenizer_summary.texts_to_sequences(df_summ['summ'])
data_summary = pad_sequences(sequences_summary, maxlen=MAX_LEN)

data_summary

# EMBEDDINGS
embeddings_index = dict()
f = open('/Users/manjari/USC_coursework/csci-544/project_word_embeddings/glove.6B.100d.txt')
for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()
print('Loaded %s word vectors.' % len(embeddings_index))

# create a weight matrix for words in training docs
vocabulary_size = len(tokenizer.word_index) + 1
print("sumvocab sizemmmmmmmmmmm",vocabulary_size)
embedding_matrix = np.zeros((vocabulary_size, 100))
for word, index in tokenizer.word_index.items():
    if index > vocabulary_size - 1:
        break
    else:
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[index] = embedding_vector

# create a weight matrix for words in training docs
summary_vocab_size = len(tokenizer_summary.word_index) + 1
print("summmmmmmmmmmmm",summary_vocab_size)
summary_embedding_matrix = np.zeros((summary_vocab_size, 100))
for word, index in tokenizer_summary.word_index.items():
    if index > summary_vocab_size - 1:
        break
    else:
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            summary_embedding_matrix[index] = embedding_vector

print(summary_embedding_matrix.shape)

###

"""##Comparison Summaries"""
temp_dict = df_summ.set_index('Movie')['summ'].to_dict()
comparison_summary_dict = {}
for movie, wordlist in temp_dict.items():
    
    wordlist = re.sub(r"<SOS> ", "", wordlist)
    wordlist = re.sub(r" <EOS>", "", wordlist)

    comparison_summary_dict[movie] = wordlist


encoder_inputs = Input(shape=(None,))
en_x=  Embedding(vocabulary_size, 100,weights = [embedding_matrix] )(encoder_inputs)
encoder = LSTM(256, return_state=True)
encoder_outputs, state_h, state_c = encoder(en_x)
encoder_states = [state_h, state_c]

decoder_inputs = Input(shape=(None,))
dex =  Embedding(summary_vocab_size, 100, weights = [summary_embedding_matrix]  )
final_dex = dex(decoder_inputs)
decoder_lstm = LSTM(256, return_sequences=True, return_state=True)
decoder_outputs, _, _ = decoder_lstm(final_dex,initial_state=encoder_states)
decoder_outputs = Dense(2000,activation='tanh')(decoder_outputs)
decoder_dense = Dense(summary_vocab_size, activation='softmax')
decoder_outputs = decoder_dense(decoder_outputs)

model = Model([encoder_inputs, decoder_inputs], decoder_outputs)
# adam = optimizers.adam(lr=0.1)
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['acc'])

print(model.summary())

num_samples = len(data_summary)
decoder_output_data = np.zeros((num_samples, MAX_LEN, summary_vocab_size), dtype="int32")

for i, seqs in enumerate(data_summary):
    for j, seq in enumerate(seqs):
        if j > 0:
            decoder_output_data[i][j][seq] = 1
print("decoder_output_data ", decoder_output_data.shape)

print("data_shape," , data.shape)
movie_train, movie_test, sum_train, sum_test = train_test_split(data, data_summary, test_size=0.2)
train_num = movie_train.shape[0]
target_train = decoder_output_data[:train_num]
target_test = decoder_output_data[train_num:]

history = model.fit([movie_train, sum_train],target_train,epochs=5 ,batch_size=batch_size,validation_data=([movie_test, sum_test], target_test))
scores = model.evaluate([movie_test, sum_test], target_test, verbose=0)
print(scores)

# # INFERENCE

encoder_model_inf = Model(encoder_inputs, encoder_states)


decoder_state_input_H = Input(shape=(latent_dim,))
decoder_state_input_C = Input(shape=(latent_dim,))

decoder_state_inputs = [decoder_state_input_H, decoder_state_input_C]
decoder_inputs_single = Input(shape=(1,))
decoder_embedding_layer=Embedding(summary_vocab_size, 100, weights = [summary_embedding_matrix]  )
decoder_embedding = decoder_embedding_layer(decoder_inputs_single)
decoder_outputs, decoder_state_h, decoder_state_c = decoder_lstm(decoder_embedding, initial_state=decoder_state_inputs)
decoder_states = [decoder_state_h, decoder_state_c]

decoder_outputs = Dense(2000,activation='tanh')(decoder_outputs)
decoder_dense = Dense(summary_vocab_size, activation='softmax')
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model_inf = Model([decoder_inputs_single]+decoder_state_inputs, [decoder_outputs] + decoder_states)


# #reversse the input/target sequence
word2idx_inputs = tokenizer.word_index
word2idx_outputs = tokenizer_summary.word_index
# print(len(word2idx_outputs.keys()))

reverse_input_word_index = {v:k for k, v in word2idx_inputs.items()}
reverse_target_word_index = {v:k for k, v in word2idx_outputs.items()}

def translate_sentence(input_seq):
    states_value = encoder_model_inf.predict(input_seq)
    target_seq = np.zeros((1, 1))
    target_seq[0, 0] = word2idx_outputs["sos"]
    eos = word2idx_outputs["eos"]
    output_sentence = []

    for _ in range(100):
        output_tokens, h, c = decoder_model_inf.predict([target_seq] + states_value)
        idx = np.argmax(output_tokens[0, 0, :])

        if eos == idx:
            break
        word = ''
        if idx > 0:
            word = reverse_input_word_index[idx]
            output_sentence.append(word)

        target_seq[0, 0] = idx
        states_value = [h, c]

    return ' '.join(output_sentence)


# test data
test_encoder_data = data[-100:]
m = len(test_encoder_data)
seq_index = 0
output_data = {}
movie_list = {i: k for i, k in enumerate(comparison_summary_dict)}
print(movie_list)
all_movie_common = list(comparison_summary_dict.keys())
while seq_index < len(test_encoder_data):
    input_text = test_encoder_data[seq_index:seq_index+1]
    #print(input_text)
    result = translate_sentence(input_text)
    #print("Result summary : ", result)
    output_data[movie_list[seq_index]] = result
    #print("Actual summary: ", comparison_summary_dict[movie_list[seq_index]])
    seq_index += 1

json_data= {"Result summary":output_data,"Actual summary":comparison_summary_dict}
with open("output_summaries_new_model2.json", 'w') as f2:
    json.dump(json_data, f2, indent=2)

